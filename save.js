(function($){
$("#start").click(start);
document.addEventListener('keypress', function(e){
  if(e.key == "Enter"){
    var count = 0
    for(a=1;a<=4;a++){
      for(b=1;b<=4;b++){
        if($("tr."+a+">td."+b+"").text()!=""){
          count++;
        }
      }
    }
    if (count==0)
    start();
    else{return 0}
  }
})
function start(){//Génération du tableau de game
  localStorage.removeItem("undo")
  $("html").css({"background-color":"rgb(241, 196, 15)"})
  $("#game").append("<header><h1 class='ctxt'>2048</h1></header>")
  $("#game").append("<table></table>");
  for(i=1;i<5;i++){
    $("table").append("<tr class="+i+"><td class='1'></td><td class='2'></td><td class='3'></td><td class='4'></tr>")
  }
  $("header").append("<p id='score' class='ctxt'>SCORE : </p> <p id ='record' class='ctxt'>RECORD : </p>")
  $("header > p").css({"display":"inline"})
  $("header").css({"max-height" : "150px", "min-height" : "150"})
  $("table").after("<input class='ctxt' type='button' value='reset' id='reset'>")
  $("table").after("<input class='ctxt' type='button' value='undo' id='undo'>")
  $("#score").append("<b class='total'> "+0+"</b>")
  if(localStorage.getItem("record")===null){$("#record").append("<b class='new'> "+0+"</b>")}
  else{$("#record").append("<b class='new'> "+localStorage.getItem("record")+"</b>")}
  $("#score").after("<h4></h4>")
  $(".ctxt").css({"margin":"auto", "font-family":"Courier New"});
  for(i=1;i<5;i++){
    $("tr."+i+"").css({"font-family":"Courier New"})
  }
  $("h1.ctxt").css({"text-align":"center", "font-size":"150px"});
  $("table").css({"table-layout":"fixed", "word-break":"break-all", "border":"1px solid black", "width":"500px", "height":"500px", "background-color":"rgb(211, 84, 0)", "margin":"auto"});
  $("td").css({"padding":"auto", "font-size":"25px","border":"1px solid black", "text-align":"center", "width":"30px", "height":"30px"});
  $("#reset").click(reset);
  $("#undo").click(function(){
    save(true)
  });
  creator();
  creator();
  // var i=0
  // for(a=1;a<=4;a++){
  //   for(b=1;b<=3;b++){
  //     $("tr."+a+">td."+b+"").text(i);
  //     i++
  //   }
  // }
  $("#start").hide();
}

function reset(){
  for(a=1;a<=4;a++){
    for(b=1;b<=4;b++){
      if($("tr."+a+">td."+b+"").text()!=""){
        $("tr."+a+">td."+b+"").text("")
      }
    }
  }
  $("b.total").text("0")
  $("h4").text("")
  $("h1").text("2048")
  $("td").css({"font-size":"25px"})
  localStorage.removeItem("undo")
  creator();
  creator();
}

document.addEventListener('keypress', function(e){//PRISE EN COMPTE DES FLECHES ET APPEL FONCTIONS MOVE
  save(false);
  var score=0;
  if(e.key == "ArrowRight" || e.key == "ArrowDown" || e.key == "ArrowLeft" || e.key == "ArrowUp"){
    var bool=false;
      if(e.key=="ArrowLeft"){
        for(x=1;x<5;x++){
          bool = moveLeft(x, bool, score);
        }
      }
      else if(e.key=="ArrowRight"){
        for(x=1;x<5;x++){
          bool = moveRight(x, bool, score);
        }
      }
      else if(e.key=="ArrowDown"){
        for(i=1;i<5;i++){
          bool = moveDown(i, bool, score);
        }
      }
      else if(e.key=="ArrowUp"){
        for(i=1;i<5;i++){
          bool = moveUp(i, bool, score);
        }
      }
      if(bool==true){
        creator()
        gameOver();
      }
  }
})

function moveLeft(x, bool, score){
  bool=cleanLeft(bool);
  for(i=1;i<4;i+=1){
    j=i+1;
    if($("tr."+x+">td."+i+"").text() !="" && $("tr."+x+">td."+j+"").text()!=""){
      if($("tr."+x+">td."+i+"").text() == $("tr."+x+">td."+j+"").text()){
        $("tr."+x+">td."+i+"").fadeOut("fast");
        $("tr."+x+">td."+i+"").fadeIn("fast").css({"background-color":"white"}).text($("tr."+x+">td."+i+"").text()*2);
        score = score + parseInt($("tr."+x+">td."+i+"").text(), 10)
        $("#score").after("<b class='temp'> +"+score+"</b>")
        $("b.total").text(parseInt($("b.total").text())+score)
        $("b.temp").fadeOut("slow")
        $("tr."+x+">td."+j+"").text("");
        bool=true;
        cleanLeft(bool);
      }
    }
  }
  return(bool);
}
function moveRight(x, bool, score){
  bool=cleanRight(bool);
  for(i=4;i>1;i-=1){
    j=i-1;
    if($("tr."+x+">td."+i+"").text() !="" && $("tr."+x+">td."+j+"").text()!=""){
      if($("tr."+x+">td."+i+"").text() == $("tr."+x+">td."+j+"").text()){
        $("tr."+x+">td."+i+"").fadeOut("fast");
        $("tr."+x+">td."+i+"").fadeIn("fast").css({"background-color":"white"}).text($("tr."+x+">td."+i+"").text()*2);
        score = score + parseInt($("tr."+x+">td."+i+"").text(), 10);
        $("#score").after("<b class='temp'> +"+score+"</b>");
        $("b.total").text(parseInt($("b.total").text())+score);
        $("b.temp").fadeOut("slow")
        $("tr."+x+">td."+j+"").text("");
        bool=true
        cleanRight(bool);
      }
    }
  }
  return(bool)
}
function moveDown(i, bool, score){
  bool=cleanDown(bool);
  for(x=4;x>1;x-=1){
    y=x-1;
    if($("tr."+x+">td."+i+"").text() !="" && $("tr."+y+">td."+i+"").text()!=""){
      if($("tr."+x+">td."+i+"").text() == $("tr."+y+">td."+i+"").text()){
        $("tr."+x+">td."+i+"").fadeOut("fast");
        $("tr."+x+">td."+i+"").fadeIn("fast").css({"background-color":"white"}).text($("tr."+x+">td."+i+"").text()*2);
        score = score + parseInt($("tr."+x+">td."+i+"").text(), 10)
        $("#score").after("<b class='temp'> +"+score+"</b>")
        $("b.total").text(parseInt($("b.total").text())+score)
        $("b.temp").fadeOut("slow")
        $("tr."+y+">td."+i+"").text("");
        bool=true
        cleanDown(bool);
      }
    }
  }
  return(bool)
}
function moveUp(i, bool, score){
  bool=cleanUp(bool);
  for(x=1;x<4;x+=1){
    y=x+1;
    if($("tr."+x+">td."+i+"").text() !="" && $("tr."+y+">td."+i+"").text()!=""){
      if($("tr."+x+">td."+i+"").text() == $("tr."+y+">td."+i+"").text()){
        $("tr."+x+">td."+i+"").fadeOut("fast");
        $("tr."+x+">td."+i+"").fadeIn("fast").css({"background-color":"white"}).text($("tr."+x+">td."+i+"").text()*2);
        score = score + parseInt($("tr."+x+">td."+i+"").text(), 10)
        $("#score").after("<b class='temp'> +"+score+"</b>")
        $("b.total").text(parseInt($("b.total").text())+score)
        $("b.temp").fadeOut("slow")
        $("tr."+y+">td."+i+"").text("");
        bool=true
        cleanUp(bool);
      }
    }
  }
  return(bool)
}

function cleanLeft(bool){
  for(k=4;k>1;k--){
    j=k-1
    if($("tr."+x+">td."+j+"").text()=="" && $("tr."+x+">td."+k+"").text()!=""){
      $("tr."+x+">td."+j+"").text($("tr."+x+">td."+k+"").text())
      $("tr."+x+">td."+k+"").text("")
      bool=true
      cleanLeft(bool);
    }
  }
  return(bool)
}
function cleanRight(bool){
  for(k=1;k<4;k++){
    j=k+1
    if($("tr."+x+">td."+j+"").text()=="" && $("tr."+x+">td."+k+"").text()!=""){
      $("tr."+x+">td."+j+"").text($("tr."+x+">td."+k+"").text())
      $("tr."+x+">td."+k+"").text("")
      bool=true
      cleanRight(bool);
    }
  }
  return(bool)
}
function cleanDown(bool){
  for(k=1;k<4;k++){
    y=k+1
    if($("tr."+y+">td."+i+"").text()=="" && $("tr."+k+">td."+i+"").text()!=""){
      $("tr."+y+">td."+i+"").text($("tr."+k+">td."+i+"").text())
      $("tr."+k+">td."+i+"").text("")
      bool=true
      cleanDown(bool);
    }
  }
  return(bool)
}
function cleanUp(bool){
  for(k=4;k>1;k--){
    y=k-1
    if($("tr."+y+">td."+i+"").text()=="" && $("tr."+k+">td."+i+"").text()!=""){
      $("tr."+y+">td."+i+"").text($("tr."+k+">td."+i+"").text())
      $("tr."+k+">td."+i+"").text("")
      bool=true
      cleanUp(bool);
    }
  }
  return(bool)
}

function creator(){
  var nbr = 1 + Math.floor(Math.random()*4);
  var nbr2 = 1 + Math.floor(Math.random()*4);
  while($("tr."+nbr+">td."+nbr2+"").text()!=""){
    var nbr = 1 + Math.floor(Math.random()*4);
    var nbr2 = 1 + Math.floor(Math.random()*4);
  }
 $("tr."+nbr+">td."+nbr2+"").fadeOut("fast");
 var coin = Math.random();
 if(coin>0.2){$("tr."+nbr+">td."+nbr2+"").fadeIn("fast", "swing").text("2");}
 else{$("tr."+nbr+">td."+nbr2+"").fadeIn("fast", "swing").text("4");}
 pimpMyTile()
}

function pimpMyTile(){
  for(a=1;a<=4;a++){
    for(b=1;b<=4;b++){
      var nbr = $("tr."+a+">td."+b+"").text()
      if(nbr%2=="0" && nbr%4!=0 && nbr%8!=0){$("tr."+a+">td."+b+"").css({"background-color":'orange', "font-size":"32px", "color":"brown"})}
      else if(nbr%4==0 && nbr%8!=0 && nbr!=""){$("tr."+a+">td."+b+"").css({"background-color":'yellow', "color":"rgb(211, 84, 0)"})}
      else if(nbr%8==0 && nbr!=""){$("tr."+a+">td."+b+"").css({"background-color":'rgb(211, 84, 0)', 'color':"yellow"})}
      else if(nbr == ""){$("tr."+a+">td."+b+"").css({"background-color":"rgb(241, 196, 15)", "color":"rgb(211, 84, 0)"})}
    }
  }
}

function gameOver(bool){
  count = 0
  for(a=1;a<=4;a++){
    for(b=1;b<=4;b++){
      var j=0
      var undo=[]
      undo.push($("tr."+a+">td."+b+"").text())
      d=b+1
      if($("tr."+a+">td."+b+"").text()!= $("tr."+a+">td."+d+"").text() && $("tr."+a+">td."+b+"").text() !="" && $("tr."+a+">td."+d+"").text()!=""){
        count++;
      }
      else if($("tr."+a+">td."+b+"").text()=="2048"){
        var i=0
        var save=[]
        var lose=["Y", " ", "U", " ", " ", "0", " ", " ", " ", "W", "  ", "N", " ", " ", "I", " "]
        for(a=1;a<=4;a++){
          for(b=1;b<=4;b++){
            save.push($("tr."+a+">td."+b+"").text())
            $("tr."+a+">td."+b+"").fadeOut('fast').fadeIn('slow').text(lose[i]);
            $("tr."+a+">td."+b+"").css({"font-size":"100px"})
            i++
          }
        }
        setTimeout(restore.bind(null,save), 5000)
      }
    }
  }
  for(a=1;a<=4;a++){
    c=a+1
    for(b=1;b<=4;b++){
      if($("tr."+a+">td."+b+"").text()!= $("tr."+c+">td."+b+"").text() && $("tr."+a+">td."+b+"").text()!="" && $("tr."+c+">td."+b+"").text()!=""){
        count++;
      }
    }
  }
  if(parseInt($("b.total").text())>parseInt($("b.new").text())){
    $("b.new").text($("b.total").text());
    localStorage.setItem("record", $("b.total").text())
  }
  if(count == 24){
  var i=0
  var save=[]
  var lose=["G", " ", "M", " ", " ", "A", " ", "E", "0", " ", "E ", " ", " ", "V", " ", "R"]
  for(a=1;a<=4;a++){
    for(b=1;b<=4;b++){
      save.push($("tr."+a+">td."+b+"").text())
      $("tr."+a+">td."+b+"").fadeOut('fast').fadeIn('slow').text(lose[i]);
      $("tr."+a+">td."+b+"").css({"font-size":"100px"})
      i++
    }
  }
  setTimeout(restore.bind(null,save), 5000)
  }
}
function restore(save){
  $("td").css({"font-size":"25px"})
  var i=0
  for(a=1;a<=4;a++){
    for(b=1;b<=4;b++){
      $("tr."+a+">td."+b+"").fadeOut('fast').fadeIn("slow").text(save[i])
      i++
    }
  }
}

function save(check){
  if(check==false){
    var array = []
    var i = 0
    for(a=1;a<=4;a++){
      for(b=1;b<=4;b++){
        array[i]=$("tr."+a+">td."+b+"").text()
        i++
      }
    }
    localStorage.setItem("undo", JSON.stringify(array))
  }
  else{
    var array = JSON.parse(localStorage.getItem("undo"))
    i = 0
    for(a=1;a<=4;a++){
      for(b=1;b<=4;b++){
        $("tr."+a+">td."+b+"").text(array[i])
        i++
      }
    }
    pimpMyTile()
  }

}

return this
})(jQuery);
